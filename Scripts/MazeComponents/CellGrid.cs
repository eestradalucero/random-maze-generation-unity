﻿using UnityEngine;
using System.Collections;

public class CellGrid {
	public int width { get; private set; }
	public int height { get; private set; }
	public Cell[,] cells {get; private set;}

	public CellGrid(int width, int height){
		this.width = width;
		this.height = height;
		cells = new Cell[width,height];
		PopulateGrid();
	}

	void PopulateGrid(){
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				cells[x,y] = new Cell((byte)WallDirection.All);
			}
		}
	}

	public bool IsCellValid(int x, int y){
		if(x >= 0 && x < width && y >= 0 && y < height)
			return true;
		return false;
	}
}
