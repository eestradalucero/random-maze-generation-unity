using UnityEngine;
using System.Collections;
using System;

public class GridVisualizer : MonoBehaviour {
	[SerializeField] GridMonobehaviour grid;
	[SerializeField] float cellDisplaySize;
	
	// Update is called once per frame
	void Update () {
		if(grid != null && grid.cellGrid != null)
			DebugDisplayCells(grid.cellGrid);
	}

	void DebugDisplayCells(CellGrid grid){
		for(int x = 0; x < grid.width; x++){
			for(int y = 0; y < grid.height; y++){
				DebugDisplayCell(x, y, cellDisplaySize, grid.cells[x,y].walls);
			}
		}
	}

	void DebugDisplayCell(int x, int y, float size, byte walls){
		Vector2 position = new Vector2(x, y);
		position *= size;
		float top = position.y + size / 2f;
		float bottom = position.y - size / 2f;
		float left = position.x - size / 2f;
		float right = position.x + size / 2f;

		Vector2 tr = new Vector2(right, top);
		Vector2 tl = new Vector2(left, top);
		Vector2 br = new Vector2(right, bottom);
		Vector2 bl = new Vector2(left, bottom);

		if((walls & (byte)WallDirection.North) == (byte)WallDirection.North)
			Debug.DrawLine(tr, tl, Color.white);

		if((walls & (byte)WallDirection.East) == (byte)WallDirection.East)
			Debug.DrawLine(tr, br, Color.white);

		if((walls & (byte)WallDirection.South) == (byte)WallDirection.South)
			Debug.DrawLine(bl, br, Color.white);

		if((walls & (byte)WallDirection.West) == (byte)WallDirection.West)
			Debug.DrawLine(bl, tl, Color.white);
	}

	IEnumerator stepByStep(){
		yield return new WaitForSeconds(1f);
		RecursiveBacktrackingMaze rbm = new RecursiveBacktrackingMaze();
		rbm.CarvePassage(0,0, grid.cellGrid);
	}
}
