﻿using UnityEngine;
using System.Collections;

public struct Cell {
	public byte walls;
	public Cell(byte _walls){
		this.walls = _walls;
	}

}

public enum WallDirection {
	None = 0,
	North = 1 << 0,
	East = 1 << 1,
	South = 1 << 2,
	West = 1 << 3,
	All = 1 << 0 |1 << 1 | 1 << 2 | 1 << 3 
}
