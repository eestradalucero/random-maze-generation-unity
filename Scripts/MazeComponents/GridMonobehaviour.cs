﻿using UnityEngine;
using System.Collections;

public class GridMonobehaviour : MonoBehaviour {
	[SerializeField] int width;
	[SerializeField] int height;
	public CellGrid cellGrid { get; private set; }

	// Use this for initialization
	void Awake () {
		cellGrid = new CellGrid(width, height);
	}


}
