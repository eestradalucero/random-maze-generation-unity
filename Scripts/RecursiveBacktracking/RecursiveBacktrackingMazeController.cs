﻿using UnityEngine;
using System.Collections;

public class RecursiveBacktrackingMazeController : MonoBehaviour {
	[SerializeField] GridMonobehaviour grid;
	[SerializeField] MazeAlgorithm mazeAlgorithm;

	// Use this for initialization
	void Start () {
		switch(mazeAlgorithm){
		case MazeAlgorithm.RecursiveBacktracker:
			RecursiveBacktrackingMaze rbm = new RecursiveBacktrackingMaze();
			rbm.CarvePassage(0,0, grid.cellGrid);
			break;
		}
	}

}

enum MazeAlgorithm {
	RecursiveBacktracker
}
