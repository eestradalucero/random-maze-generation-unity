﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RecursiveBacktrackingMaze {

	// Mazed developed after reading http://weblog.jamisbuck.org/2011/1/27/maze-generation-growing-tree-algorithm
	// by Jamis Buck

	public Cell[,] GenerateMaze(int startX, int startY, CellGrid grid){
		return null;
	}

	public void CarvePassage(int currentX, int currentY, CellGrid grid){
		
		Directions[] directions = new Directions[4];
		List<int> randomNumbers = new List<int>();
		int rn = 0;
		for(int i = 0; i < 4; i++){
			do{ 
				rn = UnityEngine.Random.Range(0, 4);
			} while(randomNumbers.Contains(rn));
			randomNumbers.Add(rn);
		}

		for(int i = 0; i < 4; i ++){
			directions[i] = new Directions( (randomNumbers[i]));
		}


		foreach(Directions d in directions){
			int newX = currentX + d.deltaX;
			int newY = currentY + d.deltaY;

			if(grid.IsCellValid(newX, newY) && grid.cells[newX, newY].walls == (byte)WallDirection.All){
				grid.cells[currentX, currentY].walls = (byte)((int)grid.cells[currentX, currentY].walls & (~(int)d.current));
				grid.cells[newX, newY].walls = (byte)(grid.cells[newX, newY].walls & (~(byte)d.opposite));
				CarvePassage(newX, newY, grid);
			}
		}

	}


}


public class Directions{
	public Directions (int direction){
		deltaX = deltaY = 0;
		if(direction == 0){
			current = WallDirection.North;
			deltaY = 1;
		} else if(direction == 1){
			current = WallDirection.South;
				deltaY = -1;
		}

		if(direction == 2){
			current = WallDirection.East;
			deltaX = 1;
		} else if(direction == 3){
			current = WallDirection.West;
				deltaX = -1;
		}

		if(current == WallDirection.North){
			opposite = WallDirection.South;
		} else if(current == WallDirection.South){
			opposite = WallDirection.North;
		} else if(current == WallDirection.East){
			opposite = WallDirection.West;
		}else if(current == WallDirection.West){
			opposite = WallDirection.East;
		}
		else{
			opposite = WallDirection.None;
		}


	}

	public int deltaX { get; private set; }
	public int deltaY { get; private set; }

	public WallDirection current { get; private set; }
	public WallDirection opposite { get; private set; }

}


public class RecursiveBacktrackingCell{
	Cell cell;
	bool visited;
	
	public RecursiveBacktrackingCell(Cell cell){
		this.cell = cell;
		visited = false;
	}
}

